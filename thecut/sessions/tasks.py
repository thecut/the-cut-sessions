# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
from thecut.sessions.management.commands import clearsessions as command
import celery


@celery.task(ignore_result=True)
def clearsessions(*args, **kwargs):
    command.Command().execute(*args, **kwargs)
