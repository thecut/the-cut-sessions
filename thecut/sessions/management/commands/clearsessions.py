# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

try:
    from django.contrib.sessions.management.commands import clearsessions
except ImportError:  # Pre-Django 1.5 compatibility
    from django.core.management.commands import cleanup as clearsessions


class Command(clearsessions.Command):

    pass
